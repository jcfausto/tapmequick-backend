configure :development, :production do
  set :datamapper_url, "sqlite3://#{File.dirname(__FILE__)}/tapmequick-scores.sqlite3"
end
configure :test do
  set :datamapper_url, "sqlite3://#{File.dirname(__FILE__)}/tapmequick-scores-test.sqlite3"
end

before do
  content_type 'application/json'
end

DataMapper.setup(:default, ENV['DATABASE_URL'] || settings.datamapper_url)

class Score
  include DataMapper::Resource

  Score.property(:id, Serial)
  Score.property(:value, Integer, :required => true)
  Score.property(:created_at, DateTime)
  Score.property(:updated_at, DateTime)

  def to_json(*a)
   {
      'id'      => self.id,
      'value'   => self.value,
      'date'    => self.updated_at
   }.to_json(*a)
  end
end

DataMapper.finalize
Score.auto_upgrade!