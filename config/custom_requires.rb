if development? 
  require 'sinatra/reloader'
  require 'debugger'
  Debugger.settings[:autoeval] = true
  Debugger.settings[:autolist] = 1
  Debugger.settings[:reload_source_on_change] = true
end

require './config/datamapper_config'

if test?
	# SimpleCov must be loaded before the Sinatra DSL
	# and the application code.
	require 'simplecov'
	SimpleCov.start

	require 'test/unit'
  require 'rack/test'
  require 'base64'
  require 'timecop'
end