require 'rubygems'
require 'sinatra'
require 'json'
require 'dm-core'
require 'dm-validations'
require 'dm-timestamps'
require 'dm-migrations'

require "#{File.dirname(__FILE__)}/custom_requires"

#manter sempre por último, em função dos testes
require "./app"
require './api/v1/tapmequick_scores.rb'