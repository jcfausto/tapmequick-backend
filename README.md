## TapmeQuick BackEnd Server

Servidor de backend para a apliação TapmeQuick. 
Criado com Sinatra e Datamapper

### Features

* RESTFul CRUD para Scores

### Install

Ruby 1.9.3 

    $ bundle
    $ rackup

### Running the tests


    $ bundle
    $ bundle exec ruby tests/tapmequick_scores_test.rb

Cobertura de teste em `coverage/index.html` 

### API

*Add a score to the database:*

    $ curl -X PUT -d '{"value": 10}' http://localhost:9292/score

*Retrieve a score from the database:*

    $ curl -X GET http://localhost:9292/score/1

### Authors

- Maintained by [Julio Cesar Fausto](jcfausto@gmail.com)

### License

Copyright (c) 2014 Julio Cesar Fausto
   
See License.txt in this directory.

