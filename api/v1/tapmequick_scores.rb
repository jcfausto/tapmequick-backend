# TODO:
# . logging
# . media types testing
# . put the database somewhere else
# . GET a range
# . multi-user with authentication

def jsonp(json)
  params[:callback] ? "#{params[:callback]}(#{json})" : json
end

# Download one note, subject and content
# Returns:
# {
#    subject : "the subject",
#    content : "wibble wibble wibble wibble""
# }
#
get '/api/v1/score/:id' do
  score = Score.get(params[:id])
  halt 404 if score.nil?
  jsonp(score.to_json)
end

# Add a note to the server, subject and content
# will give you back an id
# Body
# {
#    subject : "the subject",
#    content : "wibble wibble wibble wibble""
# }
#
# Returns
#  2
#
put '/api/v1/score' do
  # Request.body.read is destructive, make sure you don't use a puts here.
  data = JSON.parse(request.body.read)

  # Normally we would let the model validations handle this but we don't
  # have validations yet so we have to check now and after we save.
  if data.nil? || data['value'].nil? 
    halt 400
  end

  score = Score.create(
              :value => data['value'],
              :created_at => Time.now.utc,
              :updated_at => Time.now.utc)

  halt 500 unless score.save

  # PUT requests must return a Location header for the new resource
  [201, {'Location' => "/api/v1/score/#{score.id}"}, jsonp(score.to_json)]
end

# Update the content of a note, replace subject
# or content
# Body
# {
#    subject : "the subject",
#    content : "wibble wibble wibble wibble""
# }
# Subject and content are optional!
#
post '/api/v1/score/:id' do
  # Request.body.read is destructive, make sure you don't use a puts here.
  data = JSON.parse(request.body.read)
  halt 400 if data.nil?
  
  score = Score.get(params[:id])
  halt 404 if score.nil?

  %w(value).each do |key|
    if !data[key].nil? && data[key] != score[key]
      score[key] = data[key]
      score['updated_at'] = Time.now.utc
    end
  end

  halt 500 unless score.save
  jsonp(score.to_json)
end

# Remove a note entirely
#
delete 'api/v1/score/:id' do
  score = Score.get(params[:id])
  halt 404 if score.nil?
  
  halt 500 unless score.destroy
  204
end

