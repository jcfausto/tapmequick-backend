ENV['RACK_ENV'] = 'test'
require "./config/boot"

class ApplicationTest < MiniTest::Unit::TestCase
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def make_a_score(score)
    put '/api/v1/score', score.to_json
    assert_equal 201, last_response.status

    score = JSON.parse(last_response.body)
    refute_nil score['id']
    assert_equal Fixnum, score['id'].class
    assert_equal "/api/v1/score/#{score['id']}", last_response.headers['Location']

    score['id'].to_s
  end

  def retrieve_score(score_id)
    get '/api/v1/score/' + score_id
    assert_equal 200, last_response.status

    returned_score = JSON.parse(last_response.body)
    refute_nil returned_score
    
    returned_score
  end
  
  def test_add_bum_score_no_value
    put '/api/v1/score', {}.to_json
    assert_equal 400, last_response.status
  end
  
  def test_add_and_retrieve_score
    # Create a new score, then retrieve it and
    # assert that we get the same stuff back!

    score = {
        "value" => 135
    }

    score_id = make_a_score(score)

    # Retrieve the score we just created
    returned_score = retrieve_score(score_id)

    # Check we got the same score back!
    score.each_key do |k|
      refute_nil returned_score[k]
      assert_equal score[k], returned_score[k]
    end
  end

  def test_add_and_remove_score
    score = {
        "value" => 140
    }

    # Make a new score and then delete it
    score_id = make_a_score(score)
    delete '/api/v1/score/' + score_id
    assert_equal 404, last_response.status

    # Make sure that the score is gone!
    get '/api/v1/score/' + score_id
    assert_equal 200, last_response.status
  end

  def test_get_non_existent_score
    get '/api/v1/score/BLOOTYWOOTY'  # always an integer id...
    assert_equal 404, last_response.status
  end

  def test_update_non_existent_score
    score = {
        "value" => 98
    }

    post '/api/v1/score/BLOOTYWOOTY', score.to_json
    assert_equal 404, last_response.status
  end

  def test_delete_non_existent_score
    delete '/api/v1/score/BLOOTYWOOTY'
    assert_equal 404, last_response.status
  end
end
